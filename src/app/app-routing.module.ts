import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ButtonComponent } from './button/button.component';
import { TasksComponent } from './tasks/tasks.component';
import { CounterComponent } from './counter/counter.component';


const routes: Routes = [
  {
    path: 'button', component: ButtonComponent
  },
  {
    path: 'tasks', component: TasksComponent
  },
  {
    path: 'tasks/:id', component: TasksComponent
  },
  {
    path: 'counter/:id', component: CounterComponent
  },
  {
    path:'', redirectTo:'/button', pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
