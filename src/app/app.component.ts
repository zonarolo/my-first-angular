import { Component } from '@angular/core';
import { Task } from 'src/shared/models/task2.models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'my-app-nueva';

  name:string="Luis";
  surname:string="Salazar";
  age:number=25;
  sex:string="M";

  text:string="text";

  verificar:number=17;

  listOfFather: Array<string> = ['Camiseta', 'Pantalon', 'Botas'];
  listOfFather2: Array<string> = ['Manzana', 'Fresa', 'Platano'];
  listOfFather3: Array<string> = ['Lujuria', 'Gula', 'Pereza'];

  taskList: Array<any> = [{task: 'Lavar al gato', isDone: false}];


  taskList2: Array<Task> = [
    {name: 'Lavar al gato', isDone: false},
    {name: 'Sacar al perro', isDone: true},
    {name: 'Aplaudir a las 20:00', isDone: true},
    {name: 'Hacer la comida', isDone: false},
  ]
  

  constructor() { }

  confirmed($event){
    alert($event);
  }
}

