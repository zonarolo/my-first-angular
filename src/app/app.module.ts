import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NombreComponentComponent } from './nombre-component/nombre-component.component';
import { ButtonComponent } from './button/button.component';
import { IfExampleComponent } from './if-example/if-example.component';
import { Over18Component } from './over18/over18.component';
import { ForExampleComponent } from './for-example/for-example.component';
import { TasksComponent } from './tasks/tasks.component';
import { CounterComponent } from './counter/counter.component';

@NgModule({
  declarations: [
    AppComponent,
    NombreComponentComponent,
    ButtonComponent,
    IfExampleComponent,
    Over18Component,
    ForExampleComponent,
    TasksComponent,
    CounterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
