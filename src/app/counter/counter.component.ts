import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {

  counterId: number;
  constructor(private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe( (params) => {
      if (params && params.id){
        this.counterId = Number(params.id);
      }
    });
    
  }

  ngOnInit(): void {
  }

  mas(): void {
    this.counterId++;
  }

  menos(): void {
    this.counterId--;
  }
}
