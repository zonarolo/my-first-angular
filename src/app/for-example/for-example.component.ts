import { Component, OnInit, Input } from '@angular/core';
import { TaskModel } from 'src/shared/models/task.models';

@Component({
  selector: 'app-for-example',
  templateUrl: './for-example.component.html',
  styleUrls: ['./for-example.component.scss']
})
export class ForExampleComponent implements OnInit {


  public tasks: TaskModel[] = [
    {title: "Bajar la basura", done: true},
    {title: "Sacar al perro", done: false},
    {title: "Preparar el tupper", done: false},
    {title: "LLamar a los abuelos", done: true},
  ];

  @Input() list: Array<string>;
  constructor() { }

  ngOnInit(): void {
  }

}
