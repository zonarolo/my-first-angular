import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-if-example',
  templateUrl: './if-example.component.html',
  styleUrls: ['./if-example.component.scss']
})
export class IfExampleComponent implements OnInit {
  
  @Input() mostrar:boolean=true;

  constructor() { }

  ngOnInit(): void {
  }

}
