import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NombreComponentComponent } from './nombre-component.component';

describe('NombreComponentComponent', () => {
  let component: NombreComponentComponent;
  let fixture: ComponentFixture<NombreComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NombreComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NombreComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
