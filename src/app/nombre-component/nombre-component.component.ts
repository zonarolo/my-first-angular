import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-nombre-component',
  templateUrl: './nombre-component.component.html',
  styleUrls: ['./nombre-component.component.scss']
})
export class NombreComponentComponent implements OnInit {
  @Input() name:string="Juan";
  @Input() surname:string="Rolo";
  @Input() age:number=25;
  @Input() sex:string="M";
  constructor() { }

  ngOnInit(): void {
  }

}
