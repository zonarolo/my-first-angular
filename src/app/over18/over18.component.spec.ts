import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Over18Component } from './over18.component';

describe('Over18Component', () => {
  let component: Over18Component;
  let fixture: ComponentFixture<Over18Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Over18Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Over18Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
