import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-over18',
  templateUrl: './over18.component.html',
  styleUrls: ['./over18.component.scss']
})
export class Over18Component implements OnInit {
  
  @Input() verificar:number=18;

  constructor() { }

  ngOnInit(): void {
  }

}
