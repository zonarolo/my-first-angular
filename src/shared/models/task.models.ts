
export interface TaskModel {
  title: string;
  done: boolean;
}